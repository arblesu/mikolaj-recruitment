/**
 *
 *  A = [1,2,3,4,5]  B = [1,2,3,4,5] => [2,4,6,8,10]
 *  1. Creates a new cloned array
 *  2. Double the values in new array
 *  3. return  the new array
 */

function cloneAndDouble(arr) {
  //Code here
  let arr_cloned = [];
  for (let i = 0; i < arr.length; i++) {
    arr_cloned.push(arr[i] * 2);
  }
  return arr_cloned;
}

// const input = [1, 2, 3, 4, 5];
// const doubled = cloneAndDouble(input);
// console.log("Original", input);
// console.log("Doubled: ", doubled);

/**
 *
 *  Input => [10,20,30,40,50]  Output => [10, 30, 40, 50]
 *  1. Remove a value at index 'i' and return the new array without modifiying original array.
 */

function removeAtIndex(arr, index) {
  //Code here
  let arr_out = [];
  // console.log(arr_out);
  // arr_out = arr_out.splice(index, 1);
  // console.log(arr_out);
  for (let i = 0; i < arr.length; i++) {
    if (index !== i) {
      arr_out.push(arr[i]);
    }
  }
  return arr_out;
  // delete arr_out[index];
  // return arr_out;
  // a;
}

// const input = [10, 20, 30, 40, 50];
// const indexToRemove = 1;
// const removed = removeAtIndex(input);
// console.log("Original", input);
// console.log("Removed: ", removed);

/*
 * Input: List of items (products) which contains a category.
 * Return the list of items of a specific category.
 *
Input: [
  {
    category: "Water",
    name: "Coca-cola",
  },
  {
    category: "Food",
    name: "Pizza",
  },
  {
    category: "Water",
    name: "Pepsi",
  },
]

Output: [
  {
    category: "Water",
    name: "Coca-cola",
  },
  {
    category: "Water",
    name: "Pepsi",
  },
]
 */

function filterItems(itemsList, category) {
  //Code here
  let itemsListOut = [];
  for (let i = 0; i < itemsList.length; i++) {
    let currentItem = itemsList[i];
    if (currentItem.category === category) {
      itemsListOut.push(currentItem);
    }
  }

  return itemsListOut;
}

const list = [
  {
    category: "Water",
    name: "Coca-cola",
  },
  {
    category: "Food",
    name: "Pizza",
  },
  {
    category: "Water",
    name: "Pepsi",
  },
];
const filteredItems = filterItems(list, "Water");
console.log("Original", list);
console.log("Output", filteredItems);
