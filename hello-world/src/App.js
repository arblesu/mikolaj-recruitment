import React, { useState, useEffect } from "react";
import "./style.css";
import getData from "./api.js";

function App() {
  const [list, setList] = useState([]);
  const [text, settext] = useState("");
  onClick = (e) => {
    const action = {
      type: addToList,
      payload: {
        list: e.target.value,
      },
    };
    setList(appReducer(list, action));
  };
  // https://jsonplaceholder.typicode.com/todos

  useEffect(() => {
    function loadData() {
      const resp = getData();
      setText(resp);
    }
    loadData();
  }, []);

  const itemsList = ["Item 1", "Item 2", "Item 3"];

  return (
    <div>
      <form>
        <input placeholder="Item name..." />
        <button onClick="handleOnClick">Add new item</button>
      </form>
      <ul>
        {itemsList.map((item) => {
          <ul>{item}</ul>;
        })}
      </ul>
      Text:
      {text}
    </div>
  );
}

export default App;
